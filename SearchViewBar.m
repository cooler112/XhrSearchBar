//
//  SearchViewBar.m
//  HexiShopNative
//
//  Created by Apple on 15/8/18.
//  Copyright (c) 2015年 Apple. All rights reserved.
//

#import "SearchViewBar.h"

@implementation SearchViewBar

-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self initViews];
    }
    return self;
}
-(void)initViews{
    self.layer.cornerRadius = 5;
    self.layer.borderWidth = 1;
    self.layer.borderColor = UIColorFromRGB(0xf2f2f2).CGColor;
    
    self.icon = [[UIImageView alloc]initWithFrame:CGRectMake(10, 0, 20, 20)];
    if (self.width <= 20 ) {
        _icon.width = _icon.height = 18;
    }
    _icon.centerY = self.height/2;
    _icon.image = LOADIMAGECACHES(@"search_white");
    [self addSubview:_icon];
    [self addSubview:self.searchText];
}
- (UITextField *)searchText{
    if (!_searchText) {
        _searchText = [[UITextField alloc]initWithFrame:CGRectMake(35, 0, self.width-40, 25)];
        _searchText.delegate = self;
        _searchText.centerY = self.height/2;
    
        [_searchText addSubview:self.placeholderTit];
        _placeholderTit.text = @"请输入相关搜索词";
    }
    return _searchText;
}
- (UILabel *)placeholderTit{
    if (!_placeholderTit) {
        _placeholderTit = [[UILabel alloc]initWithFrame:_searchText.bounds];
        _placeholderTit.font =FONT_14;
        _placeholderTit.textColor = [UIColor whiteColor];
    }
    return _placeholderTit;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField{
      _placeholderTit.hidden = YES;
    if ([_delegate respondsToSelector:@selector(searchViewDidBeginSearching:)]) {
        [_delegate searchViewDidBeginSearching:self];
    }
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    if ([textField.text hasValue]) {
        _placeholderTit.hidden = YES;
    }else{
        _placeholderTit.hidden = NO;
    }
}

@end
