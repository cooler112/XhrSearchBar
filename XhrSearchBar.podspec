Pod::Spec.new do |s|
    s.name         = 'XhrSearchBar'
    s.version      = '0.0.1'
    s.summary      = 'An easy way to use pull-to-refresh'
    s.homepage     = 'https://baidu.com'
    s.license      = 'MIT'
    s.authors      = {'Cooler112' => 'xhrrongshuai@126.com'}
    s.platform     = :ios, '6.0'
    s.source       = {:git => 'https://gitee.com/cooler112/XhrSearchBar.git', :tag => s.version}
    s.source_files = '*.{h,m}'
 #   s.resource     = 'MJRefresh/MJRefresh.bundle'
    s.requires_arc = false
end 