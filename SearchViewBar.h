//
//  SearchViewBar.h
//  HexiShopNative
//
//  Created by Apple on 15/8/18.
//  Copyright (c) 2015年 Apple. All rights reserved.
//

#import <UIKit/UIKit.h>
@class SearchViewBar;
@protocol SearchViewBarDelegate <NSObject>
@optional
- (void)searchViewDidBeginSearching:(SearchViewBar *)SearchViewBar;
@end

@interface SearchViewBar : UIView <UITextFieldDelegate>
@property(nonatomic,strong)UITextField * searchText;
@property(nonatomic,strong)UILabel * placeholderTit;
@property(nonatomic,strong)UIImageView * icon;
@property(nonatomic,assign)id <SearchViewBarDelegate>delegate;
@end
